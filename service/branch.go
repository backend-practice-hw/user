package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbU "user_service/genproto/user_service"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/storage"
)

type branchService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewBranchService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *branchService {
	return &branchService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (b *branchService) Create(ctx context.Context, branch *pbU.Branch) (*pbU.Branch, error) {
	resp, err := b.storage.Branch().Create(ctx, branch)
	if err != nil {
		b.log.Error("error in service layer while creating branch", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (b *branchService) Get(ctx context.Context, key *pbU.PrimaryKey) (*pbU.Branch, error) {
	return b.storage.Branch().Get(ctx, key)
}

func (b *branchService) GetList(ctx context.Context, request *pbU.BranchRequest) (*pbU.BranchResponse, error) {
	return b.storage.Branch().GetList(ctx, request)
}

func (b *branchService) Update(ctx context.Context, branch *pbU.Branch) (*pbU.Branch, error) {
	return b.storage.Branch().Update(ctx, branch)
}

func (b *branchService) Delete(ctx context.Context, key *pbU.PrimaryKey) (*empty.Empty, error) {
	return b.storage.Branch().Delete(ctx, key)
}

func (b *branchService) GetBranchesList(ctx context.Context, response *pbU.Response) (*pbU.BranchResponse, error) {
	resp, err := b.storage.Branch().GetBranchesList(ctx, response)

	if err != nil {
		b.log.Error("error in service layer while getting branches data", logger.Error(err))
		return nil, err
	}

	return resp, nil
}
