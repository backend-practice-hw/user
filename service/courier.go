package service

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	pbU "user_service/genproto/user_service"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/pkg/security"
	"user_service/storage"
)

type courierService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewCourierService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *courierService {
	return &courierService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (b *courierService) Create(ctx context.Context, request *pbU.Courier) (*pbU.Courier, error) {
	return b.storage.Courier().Create(ctx, request)
}

func (b *courierService) Get(ctx context.Context, key *pbU.PrimaryKey) (*pbU.Courier, error) {
	return b.storage.Courier().Get(ctx, key)
}

func (b *courierService) GetList(ctx context.Context, request *pbU.CourierRequest) (*pbU.CourierResponse, error) {
	return b.storage.Courier().GetList(ctx, request)
}

func (b *courierService) Update(ctx context.Context, request *pbU.Courier) (*pbU.Courier, error) {
	return b.storage.Courier().Update(ctx, request)
}

func (b *courierService) Delete(ctx context.Context, key *pbU.PrimaryKey) (*empty.Empty, error) {
	return b.storage.Courier().Delete(ctx, key)
}

func (b *courierService) GetOldPassword(ctx context.Context, key *pbU.Msg) (*pbU.Password, error) {
	return b.storage.Courier().GetOldPassword(ctx, key)
}

func (b *courierService) ChangePassword(ctx context.Context, request *pbU.PasswordRequest) (*pbU.PrimaryKey, error) {
	oldPassword, err := b.services.CourierService().GetOldPassword(ctx, &pbU.Msg{Login: request.Login})
	if err != nil {
		b.log.Error("error is while getting old password", logger.Error(err))
		return nil, err
	}

	result := security.CompareHashAndPassword(oldPassword.OldPassword, request.OldPassword)
	if !result {
		fmt.Println(oldPassword.OldPassword, "req", request.OldPassword)
		fmt.Println(result)
		b.log.Error("error is while comparing passwords", logger.Error(err))
		return nil, err
	}

	msg, err := b.storage.Courier().ChangePassword(ctx, request)
	if err != nil {
		b.log.Error("error is while changing password", logger.Error(err))
		return nil, err
	}

	return msg, nil
}
