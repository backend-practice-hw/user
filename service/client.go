package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbU "user_service/genproto/user_service"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/storage"
)

type clientService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewClientService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *clientService {
	return &clientService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (c *clientService) Create(ctx context.Context, request *pbU.Client) (*pbU.Client, error) {
	return c.storage.Client().Create(ctx, request)
}

func (c *clientService) Get(ctx context.Context, key *pbU.PrimaryKey) (*pbU.Client, error) {
	return c.storage.Client().Get(ctx, key)
}

func (c *clientService) GetList(ctx context.Context, request *pbU.ClientRequest) (*pbU.ClientResponse, error) {
	return c.storage.Client().GetList(ctx, request)
}

func (c *clientService) Update(ctx context.Context, request *pbU.Client) (*pbU.Client, error) {
	return c.storage.Client().Update(ctx, request)
}

func (c *clientService) Delete(ctx context.Context, key *pbU.PrimaryKey) (*empty.Empty, error) {
	return c.storage.Client().Delete(ctx, key)
}
