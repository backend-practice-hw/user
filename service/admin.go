package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbU "user_service/genproto/user_service"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/pkg/security"
	"user_service/storage"
)

type adminService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewAdminService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *adminService {
	return &adminService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (b *adminService) Create(ctx context.Context, admin *pbU.Admin) (*pbU.Admin, error) {
	return b.storage.Admin().Create(ctx, admin)
}

func (b *adminService) Get(ctx context.Context, key *pbU.PrimaryKey) (*pbU.Admin, error) {
	return b.storage.Admin().Get(ctx, key)
}

func (b *adminService) GetList(ctx context.Context, request *pbU.AdminRequest) (*pbU.AdminResponse, error) {
	return b.storage.Admin().GetList(ctx, request)
}

func (b *adminService) Update(ctx context.Context, admin *pbU.Admin) (*pbU.Admin, error) {
	return b.storage.Admin().Update(ctx, admin)
}

func (b *adminService) Delete(ctx context.Context, key *pbU.PrimaryKey) (*empty.Empty, error) {
	return b.storage.Admin().Delete(ctx, key)
}

func (b *adminService) GetOldPassword(ctx context.Context, key *pbU.Msg) (*pbU.Password, error) {
	return b.storage.Admin().GetOldPassword(ctx, key)
}

func (b *adminService) ChangePassword(ctx context.Context, request *pbU.PasswordRequest) (*pbU.PrimaryKey, error) {
	oldPassword, err := b.services.AdminService().GetOldPassword(ctx, &pbU.Msg{Login: request.Login})
	if err != nil {
		b.log.Error("error is while getting old password", logger.Error(err))
		return nil, err
	}
	result := security.CompareHashAndPassword(oldPassword.OldPassword, request.OldPassword)
	if !result {
		b.log.Error("error is while comparing passwords", logger.Error(err))
		return nil, err
	}

	msg, err := b.storage.Admin().ChangePassword(ctx, request)
	if err != nil {
		b.log.Error("error is while changing password", logger.Error(err))
		return nil, err
	}

	return msg, nil
}
