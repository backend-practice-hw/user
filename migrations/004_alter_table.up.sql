alter table if exists branches alter column work_hour_start type time,
    alter column work_hour_end type time;
