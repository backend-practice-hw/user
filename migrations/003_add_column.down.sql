alter table if exists branches drop column work_hour_start;

alter table if exists branches drop column work_hour_end;

create type work_hour_enum as enum (
   "start",
    "end"
);

alter table if exists branches add column work_hour enum;