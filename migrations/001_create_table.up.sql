CREATE TYPE "discount_type_enum" AS ENUM (
    'sum',
    'percent'
);

CREATE TYPE "work_hour_enum" AS ENUM (
    'start',
    'end'
);

CREATE TABLE "clients" (
                           "id" uuid PRIMARY KEY NOT NULL,
                           "first_name" varchar(100),
                           "last_name" varchar(100),
                           "phone" varchar(13) UNIQUE NOT NULL,
                           "photo" text,
                           "date_of_birth" timestamp,
                           "last_ordered_date" timestamp DEFAULT 'now()',
                           "total_orders_sum" float,
                           "total_orders_count" int,
                           "discount_type" discount_type_enum,
                           "discount_amount" float,
                           "created_at" timestamp default 'Now()',
                           "updated_at" timestamp default 'Now()',
                           "deleted_at" integer default 0
);

CREATE TABLE "branches" (
                            "id" uuid PRIMARY KEY NOT NULL,
                            "name" varchar(100),
                            "phone" varchar(13) UNIQUE NOT NULL,
                            "photo" text,
                            "delivery_tariff_id" uuid,
                            "work_hour" work_hour_enum,
                            "address" text,
                            "destination" text,
                            "active" bool,
                            "created_at" timestamp default 'Now()',
                            "updated_at" timestamp default 'Now()',
                            "deleted_at" integer default 0
);

CREATE TABLE "couriers" (
                            "id" uuid PRIMARY KEY,
                            "first_name" varchar(100),
                            "last_name" varchar(100),
                            "phone" varchar(13) UNIQUE,
                            "active" bool ,
                            "login" varchar(100),
                            "password" varchar(10),
                            "max_order_count" int,
                            "branch_id" uuid,
                            "created_at" timestamp default 'Now()',
                            "updated_at" timestamp default 'Now()',
                            "deleted_at" integer default 0
);

CREATE TABLE "admins" (
                          "id" uuid PRIMARY KEY,
                          "first_name" varchar(100),
                          "last_name" varchar(100),
                          "phone" varchar(13) UNIQUE,
                          "active" bool,
                          "login" varchar(100),
                          "password" varchar(10),
                          "created_at" timestamp default 'Now()',
                          "updated_at" timestamp default 'Now()',
                          "deleted_at" integer default 0
);

ALTER TABLE "couriers" ADD FOREIGN KEY ("branch_id") REFERENCES "branches" ("id");
