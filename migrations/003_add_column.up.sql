alter table if exists branches drop column work_hour;

alter table if exists branches add column work_hour_start timestamp;
alter table if exists branches add column work_hour_end timestamp;