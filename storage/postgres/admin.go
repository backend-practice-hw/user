package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbU "user_service/genproto/user_service"
	"user_service/pkg/logger"
	"user_service/pkg/security"
)

type adminRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewAdminRepo(db *pgxpool.Pool, log logger.ILogger) *adminRepo {
	return &adminRepo{
		db:  db,
		log: log,
	}
}

func (c *adminRepo) Create(ctx context.Context, request *pbU.Admin) (*pbU.Admin, error) {
	admin := pbU.Admin{}
	hashedPassword, err := security.HashPassword(request.Password)
	if err != nil {
		c.log.Error("error is while hashing password", logger.Error(err))
		return nil, err
	}

	query := `insert into admins(id, first_name, last_name, phone, active, login, password)
                         values($1, $2, $3, $4, $5, $6, $7)
    returning id, first_name, last_name, phone, active,
                created_at::text, updated_at::text`
	if err := c.db.QueryRow(ctx, query,
		uuid.New(),
		request.GetFirstName(),
		request.GetLastName(),
		request.GetPhone(),
		request.GetActive(),
		request.GetLogin(),
		hashedPassword,
	).Scan(
		&admin.Id,
		&admin.FirstName,
		&admin.LastName,
		&admin.Phone,
		&admin.Active,
		&admin.CreatedAt,
		&admin.UpdatedAt,
	); err != nil {
		c.log.Error("error is while inserting into admins", logger.Error(err))
		return nil, err
	}
	return &admin, nil
}

func (c *adminRepo) Get(ctx context.Context, key *pbU.PrimaryKey) (*pbU.Admin, error) {
	admin := pbU.Admin{}
	query := `select id, first_name, last_name, phone, active, login, password, created_at::text, updated_at::text 
                                     from admins where id = $1 and deleted_at = 0`
	if err := c.db.QueryRow(ctx, query, key.GetId()).Scan(
		&admin.Id,
		&admin.FirstName,
		&admin.LastName,
		&admin.Phone,
		&admin.Active,
		&admin.Login,
		&admin.Password,
		&admin.CreatedAt,
		&admin.UpdatedAt,
	); err != nil {
		c.log.Error("error is while selecting admin by id", logger.Error(err))
		return nil, err
	}
	return &admin, nil
}

func (c *adminRepo) GetList(ctx context.Context, request *pbU.AdminRequest) (*pbU.AdminResponse, error) {
	var (
		count                     = 0
		admins                    []*pbU.Admin
		query, filter, countQuery string
		offset                    = (request.GetPage() - 1) * request.GetLimit()
		pagination                string
	)

	pagination += ` ORDER BY created_at desc LIMIT $1 OFFSET $2`

	if request.GetFirstName() != "" {
		filter += fmt.Sprintf(` and first_name ilike '%s'`, request.GetFirstName())
	}
	if request.GetLastName() != "" {
		filter += fmt.Sprintf(` and last_name ilike '%s'`, request.GetLastName())
	}
	if request.GetPhone() != "" {
		filter += fmt.Sprintf(` and phone = '%s'`, request.GetPhone())
	}
	if request.GetFromCreatedAt() != "" && request.GetToCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at between '%s' and '%s'`, request.GetFromCreatedAt(), request.GetToCreatedAt())
	}
	if request.GetFromCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at <= '%s'`, request.GetFromCreatedAt())
	}
	if request.GetToCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at >= '%s'`, request.GetToCreatedAt())
	}

	countQuery = `select count(1) from admins where deleted_at = 0 ` + filter

	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		fmt.Println("error is while scanning count", err.Error())
		return &pbU.AdminResponse{}, err
	}

	query = `select id, first_name, last_name, phone, active, login, password,
                created_at::text, updated_at::text from admins where deleted_at = 0  ` + filter + pagination
	rows, err := c.db.Query(ctx, query, request.GetLimit(), offset)
	if err != nil {
		fmt.Println("error is while selecting * from branches", err.Error())
		return &pbU.AdminResponse{}, err
	}

	for rows.Next() {
		admin := pbU.Admin{}
		if err := rows.Scan(
			&admin.Id,
			&admin.FirstName,
			&admin.LastName,
			&admin.Phone,
			&admin.Active,
			&admin.Login,
			&admin.Password,
			&admin.CreatedAt,
			&admin.UpdatedAt,
		); err != nil {
			fmt.Println("error is while scanning branch", err.Error())
			return &pbU.AdminResponse{}, err
		}
		admins = append(admins, &admin)
	}

	return &pbU.AdminResponse{
		Admins: admins,
		Count:  int32(count),
	}, nil
}

func (c *adminRepo) Update(ctx context.Context, request *pbU.Admin) (*pbU.Admin, error) {
	admin := pbU.Admin{}
	query := `update admins set first_name = $1, last_name = $2, phone = $3, login = $4, password = $5, active = $6, 
                   updated_at = now() where id = $7 and deleted_at = 0 
                   returning id, first_name, last_name, phone, active, login,  password, created_at::text, updated_at::text`
	if err := c.db.QueryRow(ctx, query,
		request.GetFirstName(),
		request.GetLastName(),
		request.GetPhone(),
		request.GetLogin(),
		request.GetPassword(),
		request.GetActive(),
		request.GetId(),
	).Scan(
		&admin.Id,
		&admin.FirstName,
		&admin.LastName,
		&admin.Phone,
		&admin.Active,
		&admin.Login,
		&admin.Password,
		&admin.CreatedAt,
		&admin.UpdatedAt,
	); err != nil {
		c.log.Error("error is while updating admins", logger.Error(err))
		return nil, err
	}
	return &admin, nil
}

func (c *adminRepo) Delete(ctx context.Context, key *pbU.PrimaryKey) (*empty.Empty, error) {
	query := `update admins set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := c.db.Exec(ctx, query, key.GetId())
	if err != nil {
		fmt.Println("error is while deleting admin", logger.Error(err))
		return nil, err
	}
	return nil, nil
}

func (c *adminRepo) GetOldPassword(ctx context.Context, key *pbU.Msg) (*pbU.Password, error) {
	var oldPassword string
	query := `select password from admins where login = $1`
	if err := c.db.QueryRow(ctx, query, key.GetLogin()).Scan(&oldPassword); err != nil {
		c.log.Error("error is while getting old password", logger.Error(err))
		return nil, err
	}
	return &pbU.Password{OldPassword: oldPassword}, nil
}

func (c *adminRepo) ChangePassword(ctx context.Context, key *pbU.PasswordRequest) (*pbU.PrimaryKey, error) {
	var id string
	query := `update admins set password = $2 where login = $1 returning id`
	if err := c.db.QueryRow(ctx, query, key.GetLogin(), key.GetNewPassword()).Scan(&id); err != nil {
		c.log.Error("error is while updating password", logger.Error(err))
		return nil, err
	}

	return &pbU.PrimaryKey{Id: id}, nil
}
