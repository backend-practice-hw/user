package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbU "user_service/genproto/user_service"
	"user_service/pkg/logger"
	"user_service/pkg/security"
)

type courierRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewCourierRepo(db *pgxpool.Pool, log logger.ILogger) *courierRepo {
	return &courierRepo{
		db:  db,
		log: log,
	}
}

func (c *courierRepo) Create(ctx context.Context, request *pbU.Courier) (*pbU.Courier, error) {
	courier := pbU.Courier{}
	hashedPassword, err := security.HashPassword(request.Password)
	if err != nil {
		c.log.Error("error is while hashing password", logger.Error(err))
		return nil, err
	}
	query := `insert into couriers(id, first_name, last_name, phone, login,active, password, max_order_count, branch_id)
                         values($1, $2, $3, $4, $5, $6, $7, $8, $9)
    returning id, first_name, last_name, phone, active, max_order_count, branch_id,
                created_at::text, updated_at::text`
	if err := c.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.GetFirstName(),
		request.GetLastName(),
		request.GetPhone(),
		request.GetLogin(),
		request.GetActive(),
		hashedPassword,
		request.GetMaxOrderCount(),
		request.GetBranchId(),
	).Scan(
		&courier.Id,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Active,
		&courier.MaxOrderCount,
		&courier.BranchId,
		&courier.CreatedAt,
		&courier.UpdatedAt,
	); err != nil {
		c.log.Error("error is while inserting into courier", logger.Error(err))
		return nil, err
	}
	return &courier, nil
}

func (c *courierRepo) Get(ctx context.Context, key *pbU.PrimaryKey) (*pbU.Courier, error) {
	courier := pbU.Courier{}
	query := `select id, first_name, last_name, phone, login, active, password, max_order_count, branch_id,
                created_at::text, updated_at::text 
                         from couriers where id = $1 and deleted_at = 0`
	if err := c.db.QueryRow(ctx, query, key.GetId()).Scan(
		&courier.Id,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Login,
		&courier.Active,
		&courier.Password,
		&courier.MaxOrderCount,
		&courier.BranchId,
		&courier.CreatedAt,
		&courier.UpdatedAt,
	); err != nil {
		c.log.Error("error is while selecting courier by id", logger.Error(err))
		return nil, err
	}
	return &courier, nil
}

func (c *courierRepo) GetList(ctx context.Context, request *pbU.CourierRequest) (*pbU.CourierResponse, error) {
	var (
		count                     = 0
		couriers                  []*pbU.Courier
		query, filter, countQuery string
		offset                    = (request.GetPage() - 1) * request.GetLimit()
		pagination                string
	)

	pagination += ` ORDER BY created_at desc LIMIT $1 OFFSET $2`

	if request.GetFirstName() != "" {
		filter += fmt.Sprintf(` and first_name ilike '%s'`, request.GetFirstName())
	}
	if request.GetLastName() != "" {
		filter += fmt.Sprintf(` and last_name ilike '%s'`, request.GetLastName())
	}
	if request.GetPhone() != "" {
		filter += fmt.Sprintf(` and phone = '%s'`, request.GetPhone())
	}
	if request.GetFromCreatedAt() != "" && request.GetToCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at between '%s' and '%s'`, request.GetFromCreatedAt(), request.GetToCreatedAt())
	}
	if request.GetFromCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at <= '%s'`, request.GetFromCreatedAt())
	}
	if request.GetToCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at >= '%s'`, request.GetToCreatedAt())
	}

	countQuery = `select count(1) from couriers where deleted_at = 0 ` + filter

	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		fmt.Println("error is while scanning count", err.Error())
		return &pbU.CourierResponse{}, err
	}

	query = `select id, first_name, last_name, phone, login, active, password, max_order_count, branch_id,
                created_at::text, updated_at::text
                           from couriers where deleted_at = 0  ` + filter + pagination
	rows, err := c.db.Query(ctx, query, request.GetLimit(), offset)
	if err != nil {
		fmt.Println("error is while selecting * from couriers", err.Error())
		return &pbU.CourierResponse{}, err
	}

	for rows.Next() {
		courier := pbU.Courier{}
		if err := rows.Scan(
			&courier.Id,
			&courier.FirstName,
			&courier.LastName,
			&courier.Phone,
			&courier.Login,
			&courier.Active,
			&courier.Password,
			&courier.MaxOrderCount,
			&courier.BranchId,
			&courier.CreatedAt,
			&courier.UpdatedAt,
		); err != nil {
			fmt.Println("error is while scanning branch", err.Error())
			return &pbU.CourierResponse{}, err
		}
		couriers = append(couriers, &courier)
	}

	return &pbU.CourierResponse{
		Couriers: couriers,
		Count:    int32(count),
	}, nil
}

func (c *courierRepo) Update(ctx context.Context, request *pbU.Courier) (*pbU.Courier, error) {
	courier := pbU.Courier{}
	query := `update clients set first_name = $1, last_name = $2, phone = $3, login = $4, active = $5, 
                   password = $6, max_order_count = $7, branch_id = $8,
                   updated_at = now() where id = $9 and deleted_at = 0 
                   returning id, first_name, last_name, phone, login, active, password, max_order_count, branch_id,
                created_at::text, updated_at::text`
	if err := c.db.QueryRow(ctx, query,
		request.GetFirstName(),
		request.GetLastName(),
		request.GetPhone(),
		request.GetLogin(),
		request.GetActive(),
		request.GetPassword(),
		request.GetMaxOrderCount(),
		request.GetBranchId(),
		request.GetId(),
	).Scan(
		&courier.Id,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Login,
		&courier.Active,
		&courier.Password,
		&courier.MaxOrderCount,
		&courier.BranchId,
		&courier.CreatedAt,
		&courier.UpdatedAt,
	); err != nil {
		c.log.Error("error is while updating couriers", logger.Error(err))
		return nil, err
	}
	return &courier, nil
}

func (c *courierRepo) Delete(ctx context.Context, key *pbU.PrimaryKey) (*empty.Empty, error) {
	query := `update couriers set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := c.db.Exec(ctx, query, key.GetId())
	if err != nil {
		fmt.Println("error is while deleting courier", logger.Error(err))
		return nil, err
	}
	return nil, nil
}

func (c *courierRepo) GetOldPassword(ctx context.Context, key *pbU.Msg) (*pbU.Password, error) {
	var oldPassword string
	query := `select password from couriers where login = $1`
	if err := c.db.QueryRow(ctx, query, key.GetLogin()).Scan(&oldPassword); err != nil {
		c.log.Error("error is while getting old password", logger.Error(err))
		return nil, err
	}

	return &pbU.Password{OldPassword: oldPassword}, nil
}

func (c *courierRepo) ChangePassword(ctx context.Context, key *pbU.PasswordRequest) (*pbU.PrimaryKey, error) {
	var id string
	query := `update couriers set password = $2 where login = $1 returning id`
	if err := c.db.QueryRow(ctx, query, key.GetLogin(), key.GetNewPassword()).Scan(&id); err != nil {
		c.log.Error("error is while updating password", logger.Error(err))
		return nil, err
	}

	return &pbU.PrimaryKey{Id: id}, nil
}
