package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"time"
	pbU "user_service/genproto/user_service"
	"user_service/pkg/logger"
	"user_service/storage"
)

type branchRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewBranchRepo(db *pgxpool.Pool, log logger.ILogger) storage.IBranchStorage {
	return branchRepo{db: db, log: log}
}

func (b branchRepo) Create(ctx context.Context, branch *pbU.Branch) (*pbU.Branch, error) {
	id := uuid.New()
	createdBranch := pbU.Branch{}

	query := `insert into branches (id, name, phone, photo, delivery_tariff_id, work_hour_start, work_hour_end, address, destination, active) 
									values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
		returning id, name, phone, delivery_tariff_id,  work_hour_start::text, work_hour_end::text, address, destination, active, created_at::text, updated_at::text`

	if err := b.db.QueryRow(ctx, query,
		id,
		branch.GetName(),
		branch.GetPhone(),
		branch.GetPhoto(),
		branch.GetDeliveryTariffId(),
		branch.GetWorkHourStart(),
		branch.GetWorkHourEnd(),
		branch.GetAddress(),
		branch.GetDestination(),
		branch.GetActive()).Scan(
		&createdBranch.Id,
		&createdBranch.Name,
		&createdBranch.Phone,
		&createdBranch.DeliveryTariffId,
		&createdBranch.WorkHourStart,
		&createdBranch.WorkHourEnd,
		&createdBranch.Address,
		&createdBranch.Destination,
		&createdBranch.Active,
		&createdBranch.CreatedAt,
		&createdBranch.UpdatedAt); err != nil {
		fmt.Println("error is while inserting data", err.Error())
		return &pbU.Branch{}, err
	}

	return &createdBranch, nil
}

func (b branchRepo) Get(ctx context.Context, key *pbU.PrimaryKey) (*pbU.Branch, error) {
	branch := pbU.Branch{}
	query := `select id, name, phone, delivery_tariff_id,  work_hour_start::text, work_hour_end::text, address, destination, active,
                      created_at::text, updated_at::text from branches where id = $1 and deleted_at = 0`
	if err := b.db.QueryRow(ctx, query, key.GetId()).Scan(
		&branch.Id,
		&branch.Name,
		&branch.Phone,
		&branch.DeliveryTariffId,
		&branch.WorkHourStart,
		&branch.WorkHourEnd,
		&branch.Address,
		&branch.Destination,
		&branch.Active,
		&branch.CreatedAt,
		&branch.UpdatedAt); err != nil {
		fmt.Println("error is while selecting by id", err.Error())
		return &pbU.Branch{}, err
	}
	return &branch, nil
}

func (b branchRepo) GetList(ctx context.Context, request *pbU.BranchRequest) (*pbU.BranchResponse, error) {
	var (
		count                     = 0
		branches                  []*pbU.Branch
		query, filter, countQuery string
		offset                    = (request.GetPage() - 1) * request.GetLimit()
		pagination                string
	)

	pagination += ` ORDER BY created_at desc LIMIT $1 OFFSET $2`

	if request.GetName() != "" {
		filter += fmt.Sprintf(` and name ilike '%s'`, request.GetName())
	}
	if request.GetFromCreatedAt() != "" && request.GetToCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at between '%s' and '%s'`, request.GetFromCreatedAt(), request.GetToCreatedAt())
	}
	if request.GetFromCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at <= '%s'`, request.GetFromCreatedAt())
	}
	if request.GetToCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at >= '%s'`, request.GetToCreatedAt())
	}

	countQuery = `select count(1) from branches where deleted_at = 0 ` + filter

	if err := b.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		fmt.Println("error is while scanning count", err.Error())
		return &pbU.BranchResponse{}, err
	}

	query = `select id, name, phone, delivery_tariff_id, work_hour_start::text, work_hour_end::text, address, destination, active,
                      created_at::text, updated_at::text from branches where deleted_at = 0  ` + filter + pagination
	rows, err := b.db.Query(ctx, query, request.GetLimit(), offset)
	if err != nil {
		fmt.Println("error is while selecting * from branches", err.Error())
		return &pbU.BranchResponse{}, err
	}

	for rows.Next() {
		branch := pbU.Branch{}
		if err := rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.DeliveryTariffId,
			&branch.WorkHourStart,
			&branch.WorkHourEnd,
			&branch.Address,
			&branch.Destination,
			&branch.Active,
			&branch.CreatedAt,
			&branch.UpdatedAt); err != nil {
			fmt.Println("error is while scanning branch", err.Error())
			return &pbU.BranchResponse{}, err
		}
		branches = append(branches, &branch)
	}

	return &pbU.BranchResponse{
		Branches: branches,
		Count:    int32(count),
	}, nil
}

func (b branchRepo) Update(ctx context.Context, branch *pbU.Branch) (*pbU.Branch, error) {
	updatedBranch := pbU.Branch{}
	query := `update branches set name = $1, phone = $2, delivery_tariff_id = $3, work_hour_start = $4, work_hour_end = $5, address = $6,
                    destination = $7, active = $8, updated_at = Now() where id = $9
                    returning id, name, phone, delivery_tariff_id, work_hour_start::time, work_hour_end::time, address, destination, active,
                      created_at::text, updated_at::text`

	if err := b.db.QueryRow(ctx, query,
		branch.GetName(),
		branch.GetPhone(),
		branch.GetDeliveryTariffId(),
		branch.GetWorkHourStart(),
		branch.GetWorkHourEnd(),
		branch.GetAddress(),
		branch.GetDestination(),
		branch.GetActive(),
		branch.GetId(),
	).Scan(
		&branch.Id,
		&branch.Name,
		&branch.Phone,
		&branch.DeliveryTariffId,
		&branch.WorkHourStart,
		&branch.WorkHourEnd,
		&branch.Address,
		&branch.Destination,
		&branch.Active,
		&branch.CreatedAt,
		&branch.UpdatedAt); err != nil {
		fmt.Println("error is while updating branch", err.Error())
		return &pbU.Branch{}, err
	}

	return &updatedBranch, nil
}

func (b branchRepo) Delete(ctx context.Context, key *pbU.PrimaryKey) (*empty.Empty, error) {
	query := `update branches set deleted_at = extract(epoch from current_timestamp) where id = $1`

	_, err := b.db.Exec(ctx, query, key.GetId())
	if err != nil {
		fmt.Println("error is while deleting branches", err.Error())
		return nil, err
	}
	return nil, nil
}

func (b branchRepo) GetBranchesList(ctx context.Context, response *pbU.Response) (*pbU.BranchResponse, error) {
	var (
		branches   []*pbU.Branch
		count      int
		pagination string
		offset     = (response.GetPage() - 1) * response.GetLimit()
		date       string
	)
	pagination = ` limit $2 offset $3`

	currentTime := time.Now().Format("15:04:05")
	current1, err := time.Parse("15:04:05", currentTime)

	date = ` and $1::time between work_hour_start and work_hour_end`

	query := `select id, name, phone, delivery_tariff_id, work_hour_start::text, work_hour_end::text, address, destination, active,
        created_at::text, updated_at::text from branches where active = true` + date + pagination

	rows, err := b.db.Query(ctx, query, current1, response.GetLimit(), offset)
	if err != nil {
		b.log.Error("error is while getting branches data", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		branch := pbU.Branch{}
		if err = rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.DeliveryTariffId,
			&branch.WorkHourStart,
			&branch.WorkHourEnd,
			&branch.Address,
			&branch.Destination,
			&branch.Active,
			&branch.CreatedAt,
			&branch.UpdatedAt,
		); err != nil {
			b.log.Error("error is while scanning branch data", logger.Error(err))
			return nil, err
		}
		count++
		branches = append(branches, &branch)
	}
	return &pbU.BranchResponse{
		Branches: branches,
		Count:    int32(count),
	}, nil
}
