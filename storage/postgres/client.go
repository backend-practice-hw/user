package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbU "user_service/genproto/user_service"
	"user_service/pkg/helper"
	"user_service/pkg/logger"
)

type clientRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewClientRepo(db *pgxpool.Pool, log logger.ILogger) *clientRepo {
	return &clientRepo{
		db:  db,
		log: log,
	}
}

func (c *clientRepo) Create(ctx context.Context, request *pbU.Client) (*pbU.Client, error) {
	client := pbU.Client{}
	query := `insert into clients(id, first_name, last_name, phone, photo, date_of_birth, last_ordered_date, total_orders_sum, total_orders_count, discount_type, discount_amount)
                         values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
    returning id, first_name, last_name, phone, photo, date_of_birth::text, last_ordered_date::text, total_orders_sum, total_orders_count, discount_type, discount_amount,
                created_at::text, updated_at::text`
	if err := c.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.GetFirstName(),
		request.GetLastName(),
		request.GetPhone(),
		request.GetPhoto(),
		request.GetDateOfBirth(),
		request.GetLastOrderedDate(),
		request.GetTotalOrdersSum(),
		request.GetTotalOrdersCount(),
		request.GetDiscountType(),
		request.GetDiscountAmount(),
	).Scan(
		&client.Id,
		&client.FirstName,
		&client.LastName,
		&client.Phone,
		&client.Photo,
		&client.DateOfBirth,
		&client.LastOrderedDate,
		&client.TotalOrdersSum,
		&client.TotalOrdersCount,
		&client.DiscountType,
		&client.DiscountAmount,
		&client.CreatedAt,
		&client.UpdatedAt,
	); err != nil {
		c.log.Error("error is while inserting into clients", logger.Error(err))
		return nil, err
	}
	return &client, nil
}

func (c *clientRepo) Get(ctx context.Context, key *pbU.PrimaryKey) (*pbU.Client, error) {
	client := pbU.Client{}
	query := `select id, first_name, last_name, phone, photo, date_of_birth::text, last_ordered_date::text, total_orders_sum, total_orders_count, discount_type, discount_amount,
                created_at::text, updated_at::text from clients where id = $1 and deleted_at = 0`
	if err := c.db.QueryRow(ctx, query, key.GetId()).Scan(
		&client.Id,
		&client.FirstName,
		&client.LastName,
		&client.Phone,
		&client.Photo,
		&client.UpdatedAt,
		&client.LastOrderedDate,
		&client.TotalOrdersSum,
		&client.TotalOrdersCount,
		&client.DiscountType,
		&client.DiscountAmount,
		&client.CreatedAt,
		&client.UpdatedAt,
	); err != nil {
		c.log.Error("error is while selecting by id", logger.Error(err))
		return nil, err
	}
	return &client, nil
}

func (c *clientRepo) GetList(ctx context.Context, request *pbU.ClientRequest) (*pbU.ClientResponse, error) {
	var (
		count                     = 0
		clients                   []*pbU.Client
		query, filter, countQuery string
		offset                    = (request.GetPage() - 1) * request.GetLimit()
		pagination                string
	)

	pagination += ` ORDER BY created_at desc LIMIT $1 OFFSET $2`

	if request.GetFirstName() != "" {
		filter += fmt.Sprintf(` and first_name ilike '%s'`, request.GetFirstName())
	}
	if request.GetLastName() != "" {
		filter += fmt.Sprintf(` and last_name ilike '%s'`, request.GetLastName())
	}
	if request.GetPhone() != "" {
		filter += fmt.Sprintf(` and phone = '%s'`, request.GetPhone())
	}
	if request.GetFromCreatedAt() != "" && request.GetToCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at between '%s' and '%s'`, request.GetFromCreatedAt(), request.GetToCreatedAt())
	}
	if request.GetFromCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at <= '%s'`, request.GetFromCreatedAt())
	}
	if request.GetToCreatedAt() != "" {
		filter += fmt.Sprintf(` and created_at >= '%s'`, request.GetToCreatedAt())
	}
	if request.GetFromLastOrderedDate() != "" {
		filter += fmt.Sprintf(` and last_ordered_date <= '%s'`, request.GetFromLastOrderedDate())
	}
	if request.GetToLastOrderedDate() != "" {
		filter += fmt.Sprintf(` and last_ordered_date >= '%s'`, request.GetToLastOrderedDate())
	}
	if request.GetFromTotalOrdersSum() != 0.0 {
		filter += fmt.Sprintf(` and total_orders_sum <= %.1f`, request.GetFromTotalOrdersSum())
	}
	if request.GetToTotalOrdersSum() != 0.0 {
		filter += fmt.Sprintf(` and total_orders_sum >= %.1f`, request.GetFromTotalOrdersSum())
	}
	if request.GetFromTotalOrdersCount() != 0.0 {
		filter += fmt.Sprintf(` and total_orders_sum <= %.1f`, request.GetToTotalOrdersSum())
	}
	if request.GetToTotalOrdersCount() != 0.0 {
		filter += fmt.Sprintf(` and discount_amount >= %.1f`, request.GetFromDiscountAmount())
	}
	if request.GetFromDiscountAmount() != 0.0 {
		filter += fmt.Sprintf(` and discount_amount <= %.1f`, request.GetFromDiscountAmount())
	}
	if request.GetToDiscountAmount() != 0.0 {
		filter += fmt.Sprintf(` and discount_amount >= %.1f`, request.GetToDiscountAmount())
	}
	countQuery = `select count(1) from clients where deleted_at = 0 ` + filter

	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		fmt.Println("error is while scanning count", err.Error())
		return &pbU.ClientResponse{}, err
	}

	query = `select id, first_name, last_name, phone, photo, date_of_birth::text, last_ordered_date::text, total_orders_sum, total_orders_count, discount_type, discount_amount,
                created_at::text, updated_at::text from clients where deleted_at = 0  ` + filter + pagination
	rows, err := c.db.Query(ctx, query, request.GetLimit(), offset)
	if err != nil {
		fmt.Println("error is while selecting * from branches", err.Error())
		return &pbU.ClientResponse{}, err
	}

	for rows.Next() {
		client := pbU.Client{}
		if err := rows.Scan(
			&client.Id,
			&client.FirstName,
			&client.LastName,
			&client.Phone,
			&client.Photo,
			&client.UpdatedAt,
			&client.LastOrderedDate,
			&client.TotalOrdersSum,
			&client.TotalOrdersCount,
			&client.DiscountType,
			&client.DiscountAmount,
			&client.CreatedAt,
			&client.UpdatedAt); err != nil {
			fmt.Println("error is while scanning branch", err.Error())
			return &pbU.ClientResponse{}, err
		}
		clients = append(clients, &client)
	}

	return &pbU.ClientResponse{
		Clients: clients,
		Count:   int32(count),
	}, nil
}

func (c *clientRepo) Update(ctx context.Context, request *pbU.Client) (*pbU.Client, error) {
	var (
		resp   = pbU.Client{}
		params = make(map[string]interface{})
		query  = `update clients set `
		filter = ""
	)

	fmt.Println("request", request)
	params["id"] = request.GetId()
	fmt.Println("client id", request.GetId())

	if request.GetFirstName() != "" {
		params["first_name"] = request.GetFirstName()

		filter += " first_name = @first_name,"
	}

	if request.LastName != "" {
		params["last_name"] = request.GetLastName()

		filter += " last_name = @last_name,"
	}

	if request.GetPhone() != "" {
		params["phone"] = request.GetPhone()

		filter += " phone = @phone,"
	}

	if request.GetTotalOrdersSum() != 0.0 {
		params["total_orders_sum"] = request.GetTotalOrdersSum()

		filter += " total_orders_sum = @total_orders_sum,"
	}

	if request.GetTotalOrdersCount() != 0.0 {
		params["total_orders_count"] = request.GetTotalOrdersCount()

		filter += " total_orders_count = @total_orders_count,"
	}

	if request.GetDiscountType() != "" {
		params["discount_type"] = request.GetDiscountType()

		filter += " discount_type = @discount_type,"
	}

	if request.GetDiscountAmount() != 0.0 {
		params["discount_amount"] = request.GetDiscountAmount()

		filter += " discount_amount = @discount_amount,"
	}

	query += filter + ` last_ordered_date = now(), updated_at = now() where deleted_at = 0 and id = @id returning id, total_orders_sum, total_orders_count`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	fmt.Println("fullquery", fullQuery, "\n", args)
	if err := c.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
		&resp.TotalOrdersSum,
		&resp.TotalOrdersCount,
	); err != nil {
		return nil, err
	}

	return &resp, nil

}

func (c *clientRepo) Delete(ctx context.Context, key *pbU.PrimaryKey) (*empty.Empty, error) {
	query := `update clients set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := c.db.Exec(ctx, query, key.GetId())
	if err != nil {
		fmt.Println("error is while deleting client", logger.Error(err))
		return nil, err
	}
	return nil, nil
}
