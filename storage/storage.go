package storage

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbU "user_service/genproto/user_service"
)

type IStorage interface {
	Close()
	Branch() IBranchStorage
	Client() IClientStorage
	Admin() IAdminStorage
	Courier() ICourierStorage
}

type IBranchStorage interface {
	Create(context.Context, *pbU.Branch) (*pbU.Branch, error)
	Get(context.Context, *pbU.PrimaryKey) (*pbU.Branch, error)
	GetList(context.Context, *pbU.BranchRequest) (*pbU.BranchResponse, error)
	Update(context.Context, *pbU.Branch) (*pbU.Branch, error)
	Delete(context.Context, *pbU.PrimaryKey) (*empty.Empty, error)
	GetBranchesList(context.Context, *pbU.Response) (*pbU.BranchResponse, error)
}

type IClientStorage interface {
	Create(context.Context, *pbU.Client) (*pbU.Client, error)
	Get(context.Context, *pbU.PrimaryKey) (*pbU.Client, error)
	GetList(context.Context, *pbU.ClientRequest) (*pbU.ClientResponse, error)
	Update(context.Context, *pbU.Client) (*pbU.Client, error)
	Delete(context.Context, *pbU.PrimaryKey) (*empty.Empty, error)
}

type IAdminStorage interface {
	Create(context.Context, *pbU.Admin) (*pbU.Admin, error)
	Get(context.Context, *pbU.PrimaryKey) (*pbU.Admin, error)
	GetList(context.Context, *pbU.AdminRequest) (*pbU.AdminResponse, error)
	Update(context.Context, *pbU.Admin) (*pbU.Admin, error)
	Delete(context.Context, *pbU.PrimaryKey) (*empty.Empty, error)
	GetOldPassword(context.Context, *pbU.Msg) (*pbU.Password, error)
	ChangePassword(context.Context, *pbU.PasswordRequest) (*pbU.PrimaryKey, error)
}

type ICourierStorage interface {
	Create(context.Context, *pbU.Courier) (*pbU.Courier, error)
	Get(context.Context, *pbU.PrimaryKey) (*pbU.Courier, error)
	GetList(context.Context, *pbU.CourierRequest) (*pbU.CourierResponse, error)
	Update(context.Context, *pbU.Courier) (*pbU.Courier, error)
	Delete(context.Context, *pbU.PrimaryKey) (*empty.Empty, error)
	GetOldPassword(context.Context, *pbU.Msg) (*pbU.Password, error)
	ChangePassword(context.Context, *pbU.PasswordRequest) (*pbU.PrimaryKey, error)
}
