package client

import (
	"google.golang.org/grpc"
	"user_service/config"
	pbU "user_service/genproto/user_service"
	"user_service/pkg/logger"
)

type IServiceManager interface {
	BranchService() pbU.BranchServiceClient
	ClientService() pbU.ClientServiceClient
	AdminService() pbU.AdminServiceClient
	CourierService() pbU.CourierServiceClient
}
type grpcClient struct {
	branchService  pbU.BranchServiceClient
	clientService  pbU.ClientServiceClient
	adminService   pbU.AdminServiceClient
	courierService pbU.CourierServiceClient
}

func NewGrpcClient(cfg config.Config, log logger.ILogger) (IServiceManager, error) {
	conn, err := grpc.Dial(cfg.UserGrpcServiceHost+cfg.UserGrpcServicePort, grpc.WithInsecure())
	if err != nil {
		log.Error("error is while dial", logger.Error(err))
		return nil, err
	}
	return &grpcClient{
		branchService:  pbU.NewBranchServiceClient(conn),
		clientService:  pbU.NewClientServiceClient(conn),
		adminService:   pbU.NewAdminServiceClient(conn),
		courierService: pbU.NewCourierServiceClient(conn),
	}, nil
}

func (g *grpcClient) BranchService() pbU.BranchServiceClient {
	return g.branchService
}

func (g *grpcClient) ClientService() pbU.ClientServiceClient {
	return g.clientService
}

func (g *grpcClient) AdminService() pbU.AdminServiceClient {
	return g.adminService
}

func (g *grpcClient) CourierService() pbU.CourierServiceClient {
	return g.courierService
}
