package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	pbU "user_service/genproto/user_service"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/service"
	"user_service/storage"
)

func SetUpServer(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *grpc.Server {
	grpcServer := grpc.NewServer()

	pbU.RegisterBranchServiceServer(grpcServer, service.NewBranchService(storage, services, log))
	pbU.RegisterClientServiceServer(grpcServer, service.NewClientService(storage, services, log))
	pbU.RegisterAdminServiceServer(grpcServer, service.NewAdminService(storage, services, log))
	pbU.RegisterCourierServiceServer(grpcServer, service.NewCourierService(storage, services, log))

	reflection.Register(grpcServer)

	return grpcServer
}
