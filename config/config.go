package config

import (
	"fmt"
	"github.com/spf13/cast"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	PostgresHost     string
	PostgresPort     string
	PostgresUser     string
	PostgresPassword string
	PostgresDB       string

	ServiceName string
	LoggerLevel string
	Environment string

	CatalogGrpcServiceHost string
	CatalogGrpcServicePort string

	UserGrpcServiceHost string
	UserGrpcServicePort string

	OrderGrpcServiceHost string
	OrderGrpcServicePort string
}

func Load() Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println(".env not found", err)
	}

	cfg := Config{}

	cfg.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	cfg.PostgresPort = cast.ToString(getOrReturnDefault("POSTGRES_PORT", "5432"))
	cfg.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "postgres"))
	cfg.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "password"))
	cfg.PostgresDB = cast.ToString(getOrReturnDefault("POSTGRES_DB", "db"))

	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "user_service"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))
	cfg.LoggerLevel = cast.ToString(getOrReturnDefault("LOGGER_LEVEL", "debug"))

	cfg.CatalogGrpcServicePort = cast.ToString(getOrReturnDefault("CATALOG_GRPC_SERVICE_HOST", "localhost"))
	cfg.CatalogGrpcServiceHost = cast.ToString(getOrReturnDefault("CATALOG_GRPC_SERVICE_PORT", ":8080"))

	cfg.UserGrpcServiceHost = cast.ToString(getOrReturnDefault("USER_GRPC_SERVICE_HOST", "localhost"))
	cfg.UserGrpcServicePort = cast.ToString(getOrReturnDefault("USER_GRPC_SERVICE_PORT", ":8080"))

	cfg.OrderGrpcServiceHost = cast.ToString(getOrReturnDefault("ORDER_GRPC_SERVICE_HOST", "localhost"))
	cfg.OrderGrpcServicePort = cast.ToString(getOrReturnDefault("ORDER_GRPC_SERVICE_PORT", ":8080"))

	return cfg
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	value := os.Getenv(key)
	if value != "" {
		return value
	}

	return defaultValue
}
